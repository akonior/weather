package pl.itako.weather.logic.usecases

import com.nhaarman.mockitokotlin2.any
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import pl.itako.weather.logic.entities.CityWeather
import pl.itako.weather.logic.repositories.WeatherRepository

class WeatherUseCasesTest {

    @Mock
    lateinit var weatherRepository: WeatherRepository

    lateinit var weatherUseCases: WeatherUseCases

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        weatherUseCases = WeatherUseCases(weatherRepository)
    }

    @Test
    fun testWeatherUseCaseResult(): Unit = runBlocking {
        // given
        Mockito.`when`(weatherRepository.getCityWeather(any())).thenReturn(CityWeather("Berlin", "desc"))

        // when
        val weather = weatherUseCases.getCityWeather("Berlin")

        // then
        Assert.assertEquals(
                CityWeather("Berlin", "desc"),
                weather
        )
    }
}