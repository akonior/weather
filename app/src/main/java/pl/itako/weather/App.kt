package pl.itako.weather

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import pl.itako.weather.data.DataModule

class App : Application() {

    lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)

        component = DaggerAppComponent.builder()
                .dataModule(DataModule(this))
                .build()
    }
}