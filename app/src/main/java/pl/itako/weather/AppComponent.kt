package pl.itako.weather

import dagger.Component
import pl.itako.weather.data.DataModule
import pl.itako.weather.ui.list.WeatherListFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DataModule::class])
interface AppComponent {
    fun inject(app: App)
    fun inject(app: WeatherListFragment)
}