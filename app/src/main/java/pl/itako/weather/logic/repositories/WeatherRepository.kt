package pl.itako.weather.logic.repositories

import pl.itako.weather.logic.entities.CityWeather

interface WeatherRepository {
    suspend fun getCityWeather(city: String): CityWeather
}