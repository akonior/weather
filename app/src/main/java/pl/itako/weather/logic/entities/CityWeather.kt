package pl.itako.weather.logic.entities

import java.io.Serializable

data class CityWeather(
        val city: String,
        val weatherDescription: String
) : Serializable