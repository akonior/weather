package pl.itako.weather.logic.usecases

import pl.itako.weather.asyncAwait
import pl.itako.weather.logic.entities.CityWeather
import pl.itako.weather.logic.repositories.WeatherRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherUseCases @Inject constructor(private val weatherRepository: WeatherRepository) {
    suspend fun getCityWeather(city: String): CityWeather = asyncAwait {
        weatherRepository.getCityWeather(city)
    }
}