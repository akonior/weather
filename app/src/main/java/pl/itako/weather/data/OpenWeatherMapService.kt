package pl.itako.weather.data

import kotlinx.coroutines.experimental.Deferred
import pl.itako.weather.data.json.WeatherResult
import retrofit2.http.GET
import retrofit2.http.Query


interface OpenWeatherMapService {

    @GET("weather/?appid=b6907d289e10d714a6e88b30761fae22")
    fun getWeather(@Query("q") city: String): Deferred<WeatherResult>
}