package pl.itako.weather.data

import pl.itako.weather.data.json.map
import pl.itako.weather.logic.entities.CityWeather
import pl.itako.weather.logic.repositories.WeatherRepository

class WeatherRepositoryImpl(private val openWeatherMapService: OpenWeatherMapService) : WeatherRepository {
    override suspend fun getCityWeather(city: String): CityWeather {
        val weatherResult = openWeatherMapService.getWeather(city).await()
        println("### $weatherResult")
        return map(weatherResult)
    }
}