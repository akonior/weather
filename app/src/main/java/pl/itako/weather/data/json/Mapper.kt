package pl.itako.weather.data.json

import pl.itako.weather.logic.entities.CityWeather

fun map(jsonResult: WeatherResult): CityWeather = CityWeather(
        city = jsonResult.name,
        weatherDescription = jsonResult.weather[0].description
)