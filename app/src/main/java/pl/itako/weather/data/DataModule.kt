package pl.itako.weather.data

import android.app.Application
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import pl.itako.weather.logic.repositories.WeatherRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class DataModule(val context: Application) {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://samples.openweathermap.org/data/2.5/")
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideOpenWeatherMapService(retrofit: Retrofit): OpenWeatherMapService = retrofit.create(OpenWeatherMapService::class.java)

    @Provides
    @Singleton
    fun provideWeatherRepository(openWeatherMapService: OpenWeatherMapService): WeatherRepository = WeatherRepositoryImpl(openWeatherMapService)
}