package pl.itako.weather.data.json

data class WeatherResult(
        val name: String,
        val weather: List<Weather>
)

data class Weather(
        val id: Int,
        val name: String,
        val description: String
)
