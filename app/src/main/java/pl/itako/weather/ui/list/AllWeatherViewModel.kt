package pl.itako.weather.ui.list

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import kotlinx.coroutines.experimental.Job
import pl.itako.weather.launchAsync
import pl.itako.weather.logic.usecases.WeatherUseCases

class AllWeatherViewModel(private val weatherUseCases: WeatherUseCases) : ViewModel() {

    val isLoading = MutableLiveData<Boolean>()
    val isError = ObservableBoolean(false)
    val items = MutableLiveData<List<CityWeatherItemViewModel>>()

    var useCaseJob: Job? = null

    init {
        refresh()
    }

    fun refresh() {
        isLoading.value = true
        isError.set(false)
        items.value = listOf()
        useCaseJob = launchAsync {
            try {
                items.value = cities.map {
                    println("#### getting weather for $it")
                    weatherUseCases.getCityWeather(it)
                }.map {
                    println("#### got weather  $it")
                    CityWeatherItemViewModel(it)
                }
            } catch (e: Exception) {
                isError.set(true)
            }
            isLoading.value = false
        }
    }

    override fun onCleared() {
        useCaseJob?.cancel()
    }

    companion object {
        val cities = listOf("Warsaw", "Berlin", "New York", "Barcelona")
    }
}