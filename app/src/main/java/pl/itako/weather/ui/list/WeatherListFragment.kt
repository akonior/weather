package pl.itako.weather.ui.list

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.itako.weather.App
import pl.itako.weather.R
import pl.itako.weather.databinding.FragmentCitiesListBinding
import pl.itako.weather.logic.usecases.WeatherUseCases
import javax.inject.Inject

class WeatherListFragment : Fragment() {

    @Inject
    lateinit var weatherUseCases: WeatherUseCases

    lateinit var citiesAdapter: CitiesAdapter
    lateinit var binding: FragmentCitiesListBinding
    lateinit var viewModel: AllWeatherViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (context?.applicationContext as App).component.inject(this)

        viewModel = AllWeatherViewModel(weatherUseCases)

        citiesAdapter = CitiesAdapter()
        citiesAdapter.setHasStableIds(true)

        viewModel.items.observe(activity as AppCompatActivity, Observer {
            citiesAdapter.setItems(it ?: listOf())
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCitiesListBinding.inflate(inflater, container, false)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.list_title)

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = citiesAdapter
        }

        initViewModel()

        return binding.root
    }

    private fun initViewModel() {
        viewModel.isLoading.observe(this, Observer {
            binding.refresh.isRefreshing = it ?: false
        })

        binding.refresh.setOnRefreshListener {
            viewModel.refresh()
        }

        binding.viewModel = viewModel
    }
}

