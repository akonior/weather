package pl.itako.weather.ui.list

import android.arch.lifecycle.ViewModel
import pl.itako.weather.logic.entities.CityWeather

class CityWeatherItemViewModel(val cityWeather: CityWeather) : ViewModel() {
    val name = cityWeather.city
    val description = cityWeather.weatherDescription
}