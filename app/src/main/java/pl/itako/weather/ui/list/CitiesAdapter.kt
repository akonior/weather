package pl.itako.weather.ui.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.itako.weather.databinding.CityItemBinding

class CitiesAdapter : RecyclerView.Adapter<CitiesAdapter.ViewHolder>() {
    private val items: MutableList<CityWeatherItemViewModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(CityItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewModel = items.get(position)
        holder.itemBinding.viewModel = viewModel
    }

    override fun getItemCount() = items.size

    fun setItems(newItems: List<CityWeatherItemViewModel>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    class ViewHolder(val itemBinding: CityItemBinding) : RecyclerView.ViewHolder(itemBinding.root)
}