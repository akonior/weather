### Architecture

The app is using clean architecture with three main components: data, logic, ui. 
Switching threads is implemented using Kotlin coroutines.
UI part is using MVVM pattern. Whole ui logic is inside Android agnostic ViewModel class. 

### Libraries
* Main navigation inside the app is done using Navigation Architecture Component
* DI is implemented using Dagger2
* asynchronous method calls is implemented using Kotlin coroutines
* use case test is using Mockito
* network calls - Retrofit
* layouts - constraint-layout
* json mapping - Gson
